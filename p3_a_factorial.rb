require "continuation"

## Test Case for factorial:
def test_factorial
	n = 100
	raise "#{__method__} error" if rec_factorial(n) != tail_factorial(n)
	p "#{__method__} passed"
end


###########################
## Recursive Version
def rec_factorial(n)
	if n == 0
		return 1
	else
		return n * rec_factorial(n - 1)
	end
end


## Tail Recursive Version
## Acknowledgement
## This part of code is quoted from class slides
def tail_factorial(n, c = lambda{|v| v})
    	if n == 0
        	return c.call(1)
    	else
        	return tail_factorial(n-1, lambda{|v| c.call(n*v)})
    	end
end
## End quote


###########################
test_factorial
