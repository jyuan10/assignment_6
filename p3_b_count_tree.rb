require 'continuation'

## Test case:
## A naive tree clas
## Left & Right child can be a new Tree object or nil
## Each leaf conveys a val
class Tree
	attr_accessor :val, :left, :right	
	def initialize(val = nil, l = nil, r = nil)
		@val = val
		@left = l
		@right = r
	end
end

## Create a Tree like the following:
##			1
##		  /   \
##		2       3
## 	  /   \    / 	
##	 4	   5  6

$root = Tree.new(1)
$root.left = Tree.new(2)
$root.right = Tree.new(3)
$root.left.left = Tree.new(4)
$root.left.right = Tree.new(5)
$root.right.left = Tree.new(6)

def count_tree_test(root)
	#p rec_count_tree(root)
	#p tail_count_tree(root)
	raise "#{__method__} error" if rec_count_tree(root) != tail_count_tree(root)
	p "#{__method__} passed"
end

#########################################
## Recursive Version of count tree leaves
def rec_count_tree(root)
	cnt = 1
	if root.right.nil? and root.left.nil?
		return cnt
	end
	
	if !root.right.nil?
		cnt += rec_count_tree(root.right)
	end
	
	if !root.left.nil?
		cnt += rec_count_tree(root.left)
	end
	
	return cnt
end

## Tail recursive version of count tree
def tail_count_tree(root, c = lambda{|v| v})
	if root.left.nil?
		## when leaf has no left child
		# 1. no right child either -> return 1 (itself)
		return c.call(1) if root.right.nil?
		# 2. has right child -> tail recursion
		return tail_count_tree(root.right, lambda{|v| c.call(1 + v)})
	else
		## leaf has left child
		# 1. no right child
		return tail_count_tree(root.left, lambda{|v| c.call(1 + v)}) if root.right.nil?
		# 2. has both left and right child
		l = tail_count_tree(root.left, lambda{|v| c.call(v)})

		return tail_count_tree(root.right, lambda{|v| c.call(1 + l + v)})
	end
end

##########################################
count_tree_test($root)


