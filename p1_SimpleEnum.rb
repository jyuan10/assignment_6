require "continuation"

## Acknowledgement
## Partial codes are given for the following methods:
## initialize, yield, next
## For these methods my own work is labeled with comments

class SimpleEnum
	include Enumerable
	attr_accessor :index
	attr_accessor :enum_new
	attr_accessor :block
	attr_accessor :end
	attr_accessor :queue

	def initialize(enum)
		@enum_new = enum
		@index = 0
		@block = proc{|g|
			enum.each{|x| g.yield x}
		}
		
		@queue = []
		@cont_next = @cont_yield = nil

		## Own Codes
		@end = nil
		if @cont_next = callcc{|c| c}
			@block.call(self)
		end	
		return self
		## End
	end
	
	def yield(value)
		if @cont_yield = callcc{|c| c}
			## Own Codes
			@queue << value
			@cont_next.call if @cont_next
			## End
		end
	end

	def next
		#p @queue
		#p self.next?
		## Own code
		if @end
			raise "StopIteration Exception"
		end
		## end

		if @cont_next = callcc{|c|c}
			@cont_yield.call if @cont_yield
		end
		@index += 1	
		@queue.shift
	end
	
	def next?
		@end = @queue.empty?
		return !@end
	end

	def with_index(offset = 0)
		r = []
		self_new = SimpleEnum.new(@enum_new)
		while self_new.next? do
			if self_new.index < offset
				self_new.next
			else
				r << [self_new.next, self_new.index-1]
			end
		end
		## return an new Enumerator objects
		## return [element, indx] when call .next 
		return SimpleEnum.new(r)
	end
	
	def each(&block)
		r = []
		self_new = SimpleEnum.new(@enum_new)
		while self_new.next? do
			r << block.call(self_new.next)
		end
		return r
	end

end