require './p1_SimpleEnum'
require 'continuation'

def test_next
	r = []
	a = SimpleEnum.new(['a', 'b', 'c'])
  	r << a.next until !a.next?
  	raise "#{__method__} error" if r != ['a', 'b', 'c']
  	p "#{__method__} passed"
end

def test_next?
  	r = []
  	f = true
  
  	a = SimpleEnum.new(['a', 'b', 'c'])
  	r << a.next until !a.next?
  	f = a.next?

  	raise "#{__method__} error" if r != ['a', 'b', 'c'] or f
  	p "#{__method__} passed"
end

def test_with_index(offset = 1)
  	r = []
  	a = SimpleEnum.new(['a', 'b', 'c'])
  	enum = a.with_index(offset)
  	while enum.next? do
    		r << enum.next
  	end

  	raise "#{__method__} error" if r[1] != ['c', 2]
  	p "#{__method__} passed"
end

def test_each
  	a = SimpleEnum.new([1,2,3,4])
  	r = a.each{|e| e*e}
  	raise "#{__method__} error" if r != [1,4,9,16]
  	p "#{__method__} passed"
end


test_next
test_next?
test_each
test_with_index


