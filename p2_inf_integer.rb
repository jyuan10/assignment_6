require 'continuation'

## Test Case
def inf_enum_test
	r = []
	## This is the Enumerator Object
	int_enum = inf_enum
	for i in 1..100
		r << int_enum.next
	end
	raise "#{__method__} error" if r[99] != 100
	p "#{__method__} passed"
end


#####################################
## Create an Enumerator Object
## enumerates all integers
def inf_enum
	int_enum = Enumerator.new{|g|
		n = 1
	    	callcc{|c|@c = c}
    		g.yield n
    		n += 1
    		@c.call if n > 0
  	}
  	return int_enum
end

#####################################
inf_enum_test
